<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 */

namespace App;

use App\Http\Repositories\UnsubscriptionRepository;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Auth\EloquentUserProvider;
use Threefold\Middleware\Exception\AdvantageConnectionException;
use Threefold\Middleware\Exception\InvalidCustomerException;
use Threefold\Middleware\Exception\InvalidTokenException;
use Threefold\Middleware\Middleware;

/**
 * Class Unsubscriber
 *
 * @package App
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class Unsubscriber
{
    /**
     * run
     *
     * @param Middleware $middleware
     * @param UnsubscriptionRepository $unsubscriptionRepository
     */
    public function run(Middleware $middleware, UnsubscriptionRepository $unsubscriptionRepository)
    {
        $unSubscribesToBeRun = $unsubscriptionRepository->getUnsubscriptionsToBeProcessed();

        if(!empty($unSubscribesToBeRun)){

            foreach ($unSubscribesToBeRun as $unSubscribe)
            {
                //call Middleware and get the status of this unsubscribe

                //200 content
                //204 false
                // other  MiddlewareException
                //403 no token InvalidTokenException
                //500    AdvantageConnectionException

                try
                {
                    // Service returns status code 200 OK and empty body upon success
                    $advResponse = $middleware->unsubscribeCustomerSignup(
                                                                            $unSubscribe->listCode,
                                                                            $unSubscribe->emailAddress);
                    $advResponse = 'Response is Empty Assumed all Went Well';
                    $unSubscribe->status_id = 5; //complete


                } catch (InvalidCustomerException $e) {
                    $unSubscribe =  $this->updateStatusCodeForUnsubscribe($unSubscribe);
                    $advResponse = $e->getMessage();

                } catch(AdvantageConnectionException $e ) {
                    $unSubscribe =  $this->updateStatusCodeForUnsubscribe($unSubscribe);
                    $advResponse = $e->getMessage();

                } catch(ClientException $e) {
                    $unSubscribe =  $this->updateStatusCodeForUnsubscribe($unSubscribe);
                    $advResponse = $e->getMessage();
                }

                $unSubscribe->adv_response = $advResponse;
                $unsubscriptionRepository->saveUnsubscription($unSubscribe);

            }

        }

    }

    /**
     * updateStatusCodeForUnsubscribe
     *
     * @param Unsubscription $unSubscribe
     * @return mixed
     */
    private function updateStatusCodeForUnsubscribe($unSubscribe){

        switch ($unSubscribe->status_id) {
            case 1:
                $unSubscribe->status_id = 2;
                break;
            case 2:
                $unSubscribe->status_id = 3;
                break;
            case 3:
                $unSubscribe->status_id = 4;
                break;
            case 4:
                $unSubscribe->status_id = 7;
                break;
        }
        return $unSubscribe;
    }



}