<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 */

namespace App;

use App\Http\Repositories\UnsubscriptionRepository;
use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;
use Illuminate\Contracts\Config\Repository as ConfigContract;

/**
 * Class Reporter
 * @package App
 */
class Reporter
{

    /**
     * run
     *
     * @param UnsubscriptionRepository $unsubscriptionRepository
     */
    public function run(UnsubscriptionRepository $unsubscriptionRepository, Repository $config)
    {

	    $domains = DB::table('Unsubscriptions')
	                 ->select('domainName')
	                 ->groupBy('domainName')
	                 ->get();

	    $processedUnsubscribes = $unsubscriptionRepository->getProcessedUnsubscriptions($domains);

	    if(empty($processedUnsubscribes)) {
		    return;
	    }

	    $toDate = Carbon::now()->format('d-m-Y');
	    $fromDate = Carbon::now()->subWeek(1)->format('d-m-Y');
	    $toEmails = $config->get('services.agora.toemails');
	    $ccEmails = $config->get('services.agora.ccmails');

	    foreach($domains as $domain){
		    if($domain->domainName != '') {
			    $report = '<p>Successful Unsubscribes: '
			              . sizeof($processedUnsubscribes[$domain->domainName . '_successful']) . '</p>';
			    $report .= '<p>Unsuccessful Unsubscribes: '
			               . sizeof($processedUnsubscribes[$domain->domainName . '_unsuccessful']) . '</p>';
			    $subject =  'Your Weekly Unsubscriptions Report for ' . $domain->domainName . ': ' . $fromDate . ' to ' . $toDate;

			    Mail::send('emails.send', ['title' => 'Your Weekly Unsubscriptions Report for ' . $domain->domainName, 'content' => $report],
				    function ($message) use ($domain, $toEmails, $ccEmails, $subject) {
					    $message->subject($subject);
					    $message->to($toEmails[$domain->domainName]);
					    $message->cc($ccEmails);
					    $message->from('unsubs.moneyweek@gmail.com');
				    });
		    }
	    }
    }
}