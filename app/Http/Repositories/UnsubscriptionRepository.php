<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 */

namespace App\Http\Repositories;

use App\Unsubscription;

/**
 * Class UnsubscriptionRepository
 *
 * @package App\Http\Repositories
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class UnsubscriptionRepository
{
    /**
     * saveUnsubscription
     *
     * Save the record to the db using eloquent
     *
     * @param Unsubscription $unsubscription
     * @return bool
     */
    public function saveUnsubscription(Unsubscription $unsubscription)
    {
        return $unsubscription->save();
    }

    /**
     * getUnsubscriptionsByQueue
     *
     * Get unsubscription requests from db based on queue
     *
     * @param int $queue
     * @return mixed
     */
    protected function getUnsubscriptionsByQueue($queue)
    {
        //get all queues from DB
        $queue = Unsubscription::MinutesAgo(1)->ByStatus($queue)->orderBy('created_at')->get();
        return $queue;
    }

    /**
     * getUnsubscriptionsToBeProcessed
     *
     * Get unsubscription requests from db based on based on hard code criteria
     *
     * @return mixed
     */
    public function getUnsubscriptionsToBeProcessed()
    {
        //get all queues from DB
        $queueOfUnsubscribes = Unsubscription::where(     function ($query){$query->MinutesAgo(1)->ByStatus(1); })
                                ->orWhere(  function ($query){$query->MinutesAgo(15)->ByStatus(2);})
                                ->orWhere(  function ($query){$query->MinutesAgo(30)->ByStatus(3);})
                                ->orWhere(  function ($query){$query->MinutesAgo(60)->ByStatus(4);})
                                ->orderBy('created_at')->get();


        return $queueOfUnsubscribes;
    }

    /**
     * getProcessedUnsubscriptions
     *
     * Get processed unsubscription requests from db based on based on hard code criteria
     *
     * @return mixed
     */
    public function getProcessedUnsubscriptions($domains)
    {
        $processedUnsubscribes = array();

        foreach($domains as $domain) {
            if($domain->domainName != '') {
                //get all processed requests from DB
                $processedUnsubscribes[$domain->domainName . '_successful'] = Unsubscription::where(function ($query) use ($domain) {
                    $query->ThisWeek(5)->ByDomainName($domain->domainName);
                })->get();
                $processedUnsubscribes[$domain->domainName . '_unsuccessful'] = Unsubscription::where(function ($query) use ($domain) {
                    $query->ThisWeek(7)->ByDomainName($domain->domainName);
                })->get();
            }
        }
        return $processedUnsubscribes;
    }
}