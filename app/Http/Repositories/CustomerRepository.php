<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 */

namespace App\Http\Repositories;

use App\CustomerProfiles;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;


/**
 * Class CustomerRepository
 *
 * @package App\Http\Repositories
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class CustomerRepository
{
    /**
     * saveProfile
     *
     * Save the record to the db using eloquent
     *
     * @param CustomerProfiles $customerProfile
     * @return bool
     */
    public function saveProfile(CustomerProfiles $customerProfile)
    {
        return $customerProfile->save();
    }

    /**
     * getCustomerProfileById
     *
     * Get unsubscription requests from db based on queue
     *
     * @param int $id
     * @return response
     */
    public function getCustomerProfileById($id)
    {
        //get Customer from DB
        try {
            $customer = CustomerProfiles::where('identifier', '=', $id)->firstOrFail();
            return response($customer->profile, 200);

        } catch (ModelNotFoundException $ex) {
            return response('Not Found or Expired', 404);
        }
    }

    /**
     *  getCustomerProfilesByMinutesAndExcludeStatus
     *
     * Get customerProfiles from db based on minutes and processed status
     *
     * @param int $minutes
     * @param int $status1
     * @param int $status2
     * @return CustomerProfiles
     */
    public function getCustomerProfilesByMinutesAndExcludeStatus($minutes = 32, $status1 = 1, $status2 = 2)
    {
        //get Customer from DB
        $results =  CustomerProfiles::byMinutes($minutes)->excludeByProcessedStatus($status1)
            ->excludeByProcessedStatus($status2)->get();

        CustomerProfiles::byMinutes($minutes)->excludeByProcessedStatus($status1)->update(array('processed' => 2));

        return $results;

    }

    /**
     *  getCustomerProfilesByStatus
     *
     * Get customerProfiles from db based on processed status
     *
     * @param int $status
     * @return CustomerProfiles
     */
    public function getCustomerProfilesByStatus($status = 4)
    {
        //get Customer from DB
        $results =  CustomerProfiles::byProcessedStatus($status)->get();

        CustomerProfiles::byProcessedStatus($status)->update(array('processed' => 2));

        return $results;

    }
}