<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('webservice/get/{key}','WebService@getCache')->middleware('cors');
Route::match(['post','options'],'webservice/set/{key}','WebService@setCache')->middleware('cors');

Route::get('/','UnsubscribeController@invalidListCode');
Route::get('/{listname}','UnsubscribeController@unsubscribeRequestView');
Route::post('/','UnsubscribeController@processUnsubscribeRequest');
