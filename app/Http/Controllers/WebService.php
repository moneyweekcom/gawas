<?php

namespace App\Http\Controllers;

use App\CustomerProfiles;
use App\Http\Repositories\CustomerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class WebService
 * @package App\Http\Controllers
 */
class WebService extends Controller
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * WebService constructor.
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * setCache
     *
     * @param Request $request
     * @return \App\Http\Repositories\response
     */
    public function setCache(Request $request)
    {
        $identifier = $request->route('key');
        $objToStore =  $request->getContent();
        if(!empty($identifier) && !empty($objToStore)){
            $customerProfile =  CustomerProfiles::where('identifier', '=', $identifier)->first();
            if(empty($customerProfile)){
                $customerProfile = new CustomerProfiles();
                $customerProfile->setAttribute('identifier', $identifier);
            }
            $customerProfile->profile = $objToStore;
            $objToStore = json_decode($objToStore, true);
            if( isset( $objToStore['complete'] ) ) {
                $customerProfile->processed = 4;
            }
            $this->customerRepository->saveProfile($customerProfile);
        }
        return $this->getCache($identifier);

    }

    /**
     * getCache
     *
     * @param $identifier
     * @return \App\Http\Repositories\response
     */
    public function getCache($identifier)
    {
       if(!empty($identifier)) {
           return $this->customerRepository->getCustomerProfileById($identifier);
       }
    }

}
