<?php

namespace App\Http\Controllers;

use App\Http\Repositories\UnsubscriptionRepository;
use App\Unsubscription;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Config\Repository as ConfigContract;

/**
 * Class UnsubscribeController
 *
 * @package App\Http\Controllers
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class UnsubscribeController extends Controller
{
    private $config;
    private $listName;
    private $listCode;

    /**
     * UnsubscribeController constructor.
     *
     * @param ConfigContract $config
     */
    public function __construct(ConfigContract $config)
    {
        $this->config = $config;
    }


    /**
     * unsubscribeRequestView
     * Returns an Unsubscribe view
     *
     * @param Request $request
     * @param string $listCode
     * @param string $emailAddress
     * @param string $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function unsubscribeRequestView(Request $request, $listCode = '', $emailAddress = '', $response = ''){

        $this->listCode = $listCode;
        if (Input::get('emailAddress')) {
            $emailAddress = Input::get('emailAddress');
        } elseif (Input::get('email')) {
            $emailAddress = Input::get('email');
        }

       $listCodesAvailable =  $this->config->get('services.agora.middleware.validListCodes');
       $listCodesNames =  $this->config->get('services.agora.middleware.validListNames');

        $keyFound = false;
        foreach($listCodesAvailable as $key => $lcode){
            if($this->listCode == $lcode) {
                $keyFound = true;
                $this->listCode = $listCodesAvailable[$key];
                $this->listName = $listCodesNames[$key];
                break;
            }
        }
        if($keyFound !== true){
            return $this->invalidListCode();
        }
        if($request->session()->has('error')){
            $response[] = array('message'=> $request->session()->get('error'),'status' => 'error');
        }

        $template = $this->getSiteTemplatename();
        return view($template . '.base.unsubscribeRequest', array(
                                                                    'listCode' => $this->listCode,
                                                                    'listName'  => $this->listName,
                                                                    'response'  => $response,
                                                                    'emailAddress' => $emailAddress,
                                                                    'template'  => $template)
                                                                    );

    }

    /**
     * invalidListCode
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invalidListCode()
    {
        $template = $this->getSiteTemplatename();
	    return view($this->getSiteTemplatename() . '.base.invalid', array('template'=>$template, 'listCode'=> $this->listCode));
    }


    /**
     * processUnsubscribeRequest
     *
     * @param Request $request
     * @param UnsubscriptionRepository $unsubscriptionRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function processUnsubscribeRequest(Request $request, UnsubscriptionRepository $unsubscriptionRepository){

        $this->listCode = $request->get("listCode");
        $this->listName = $request->get("listName");
        $template = $this->getSiteTemplatename();

        if(!filter_var($request->get("emailAddress"), FILTER_VALIDATE_EMAIL)){
            Session::flash('error',"Invalid Email Address");
            return Redirect::back();
        }
        if(empty($request->get("listCode"))){
            Session::flash('error',"Invalid List Name");
            return Redirect::back();
        }

        $unsubscriptionRequest = new Unsubscription();
        $unsubscriptionRequest->setEmailAddress($request->get("emailAddress"))
                                ->setListCode($this->listCode)
                                ->setDomainName($template);

        $saveUnsubscription = $unsubscriptionRepository->saveUnsubscription($unsubscriptionRequest);

        if ($saveUnsubscription) {
            $message = 'Your email address has been removed from the '. $this->listName .' mailing list. ';
            $message .= 'Please allow up to 72 hours for processing. You may receive one or two issues in the meantime,
                    but no further action is required.';
            Session::flash('success', $message);
            return view('success', array('template'=>$template,'listCode'=>$this->listCode));

        } else {
            Session::flash('error',"Failed To Save");
            return Redirect::back();
        }
    }

    /**
     * getSiteTemplatename
     *
     * returns the site domain directory
     *
     * @return string
    */
    protected function getSiteTemplatename(){

        $domain = explode('.', $_SERVER["SERVER_NAME"]);
        $domain = $domain[1];
        if ( !view()->exists( $domain . '.base.unsubscribeRequest' ) ){
            $domain = 'default';
        }
        return $domain;
    }


}
