<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerProfiles extends Model
{
    use SoftDeletes;

    protected $table = 'customer_profiles';
    protected $fillable = ['identifier', 'profile'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * scopeDeleteByMinutes
     *
     * @param $query
     * @param $minutes
     * @return mixed
     */
    public function scopeByMinutes($query, $minutes)
    {
        return $query->where('created_at', '<=', Carbon::now()->subMinutes($minutes)->toDateTimeString());
    }

    /**
     * scopeExcludeByProcessedStatus
     *
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeExcludeByProcessedStatus($query, $status = 1)
    {
        return $query->where('processed', '!=', $status);
    }

    /**
     * scopeByProcessedStatus
     *
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeByProcessedStatus($query, $status = 4)
    {
        return $query->where('processed', '=', $status);
    }
}
