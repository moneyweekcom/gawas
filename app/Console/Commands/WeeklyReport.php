<?php

namespace App\Console\Commands;

use App\Http\Repositories\UnsubscriptionRepository;
use App\Reporter;
use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigContract;


/**
 * Class WeeklyReport
 * @package App\Console\Commands
 */
class WeeklyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tfs:weeklyReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly Report for unsubscriptions';

    /**
     * @var Reporter
     */
    protected $reporter;


    /**
     * @var UnsubscriptionRepository
     */
    protected $unsubscriptionRepository;

    protected $config;
    /**
     * WeeklyReport constructor.
     *
     * @param Reporter $reporter
     * @param UnsubscriptionRepository $unsubscriptionRepository
     * @param ConfigContract $config
     */
    public function __construct(Reporter $reporter, UnsubscriptionRepository $unsubscriptionRepository, ConfigContract $config)
    {
        parent::__construct();
        $this->reporter = $reporter;
        $this->unsubscriptionRepository = $unsubscriptionRepository;
        $this->config= $config;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting Weekly Report');
        $this->reporter->run($this->unsubscriptionRepository, $this->config);
        $this->info('Weekly Report Completed');
    }
}

