<?php

namespace App\Console\Commands;

use App\Http\Repositories\CustomerRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Threefold\MessageCentral\Exception\MessageCentralException;
use Threefold\MessageCentral\MessageCentralFactory;
use Psr\Log\LoggerInterface as Logger;
use Illuminate\Contracts\Config\Repository as ConfigContract;

/**
 * Class CustomerProfileWorkerProcess
 *
 * @package App\Console\Commands
 * @author John Dunne <jdunne@threefoldsystems.com>
 */

class CustomerProfileWorkerProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tfs:opiumWorker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is used to expire all records older then 32 min and send order emails via message central';


    /**
     * @var \Threefold\Middleware\MessageCentralInterface
     */
    protected $messageCentral;
    /**
     * @var
     */
    protected $customerRepository;

    /**
     * @var
     */
    protected $logger;

    /**
     * CustomerProfileWorkerProcess constructor.
     * @param CustomerRepository $customerRepository
     * @param Logger $logger
     * @param ConfigContract $config
     */
    public function __construct(CustomerRepository $customerRepository, Logger $logger,  ConfigContract $config)
    {
        parent::__construct();
        $this->customerRepository = $customerRepository;
        $messageCentralFactory = new MessageCentralFactory($logger);
        $this->logger = $logger;

        $token = $config->get('services.agora.messagecentral.token');
        $mcEnv = $config->get('services.agora.messagecentral.env');
        $orgId = $config->get('services.agora.messagecentral.orgID');
        $stack = $config->get('services.agora.messagecentral.stack');
        $this->mailID = $config->get('services.agora.messagecentral.mailID');
        $this->senderEmail = $config->get('services.agora.orderEmailSender');
        $this->messageCentral = $messageCentralFactory->create($token, $orgId, $stack, $mcEnv);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Expire anything older than 30 min
        $customers = $this->customerRepository->getCustomerProfilesByMinutesAndExcludeStatus(32, 1);

        //Anything with status 4 (order completed)
        $customersComplete = $this->customerRepository->getCustomerProfilesByStatus(4);

        if($customers->isEmpty()){
            $customers = $customersComplete;
        } else {
            $customers->merge($customersComplete);
        }

        foreach ($customers as $customer) {
            try {
                $customerProfile = $customer['profile'];

                $customerProfile = json_decode($customerProfile, true);
                if (empty($customerProfile) || empty($customerProfile['orders'])) {
                    //hard delete an empty order
                    $customer->delete();
                    continue;
                }

                // Get the template directory for the affiliate
                $template = $this->getSourceSiteTemplate(urldecode($customerProfile['sourceUrl']));

                $total = 0;
                $delivery = 0;
            } catch (\Exception $e) {
                $customer->processed = 3;
                $customer->messageCentralResponse = $e->getMessage();
            }

            //return item name for each order to load item templates in the view
            foreach ($customerProfile['orders'] as $key => $field) {

                try {
                    $itemDelivery = preg_split('/(?=\d)/', $customerProfile['orders'][$key]['delivery'], 2);
                    if (isset($itemDelivery[1])) {
                        $delivery = $delivery + floatval($itemDelivery[1]);
                    }
                    $customerProfile['orders'][$key]['subtotal'] = str_replace(',', '',
                        $customerProfile['orders'][$key]['subtotal']);
                    $total = $total + floatval(preg_split('/(?=\d)/', $customerProfile['orders'][$key]['subtotal'],
                            2)[1]);
                    $itemName = str_replace(' ', '', $field['itemName']);
                    if (view()->exists($template . '.items.' . $itemName)) {
                        $customerProfile['orders'][$key]['template'] = $itemName;
                    } else {
                        $customerProfile['orders'][$key]['template'] = 'default';
                    }
                } catch (\Exception $e) {
                    $customer->processed = 3;
                    $customer->messageCentralResponse = $e->getMessage();
                }
            }

            try {
                $currency = preg_split('/(?=\d)/', $customerProfile['orders'][0]['subtotal'], 2)[0];
                $total = $currency . ' ' . number_format((float)$total, 2, '.', '');

                $delivery = $currency . ' ' . number_format((float)$delivery, 2, '.', '');

                $emailAddress = urldecode($customerProfile['emailAddress']);
            } catch (\Exception $e) {
                $customer->processed = 3;
                $customer->messageCentralResponse = $e->getMessage();
            }

            try {
                $view = View::make(
                    $template . '.emails.emailOrder',
                    ['orders' => $customerProfile['orders'],
                        'customerName' => $customerProfile['firstName'] . ' ' . $customerProfile['lastName'],
                        'customer' => $customerProfile,
                        'email' => $emailAddress,
                        'delivery' => $delivery,
                        'total' => $total,
                        'template' => 'default']);

                $content = $view->render();
            } catch (\Exception $e) {
                $customer->processed = 3;
                $customer->messageCentralResponse = $e->getMessage();
            }

            try {
                //connect to Message Central and Send the Mail
                $content_id = $this->messageCentral->putTriggerMailing($emailAddress , $content, $this->mailID);
                $customer->messageCentralResponse = $content_id;

                //processing complete
                $customer->processed = '1';
                $customer->save();

            } catch (\Exception $e) {
                // catch errors from Message central and add to the db messageCentralResponse field
                $customer->processed = '3';
                $customer->messageCentralResponse = $e->getMessage();
                $customer->save();
            }
        }
    }

    /**
     * getSourceSiteTemplate
     *
     * returns the site domain directory
     * @param  $url
     * @return string
     */
    protected function getSourceSiteTemplate($url){

        $domain = explode('.', $url);
        $domain = $domain[1];
        if ( !view()->exists( $domain . '.emails.emailOrder' ) ){
            $domain = 'default';
        }
        return $domain;
    }

}
