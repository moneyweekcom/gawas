<?php

namespace App\Console\Commands;

use App\Http\Repositories\UnsubscriptionRepository;
use App\Unsubscriber;
use Illuminate\Console\Command;
use Threefold\Middleware\MiddlewareFactory;
use Illuminate\Contracts\Config\Repository as ConfigContract;
use Monolog\Logger;
use Monolog\Handler\LogglyHandler;

/**
 * Class UnsubWorkerProcess
 *
 * @package App\Console\Commands
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class UnsubWorkerProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tfs:queueWorker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Worker Process for Queues';

    /**
     * @var Unsubscriber
     */
    protected $unsubscriber;

    /**
     * @var UnsubscriptionRepository
     */
    protected $unsubscriptionRepository;

    /**
     * UnsubWorkerProcess constructor.
     * @param Unsubscriber $unsubscriber
     * @param UnsubscriptionRepository $unsubscriptionRepository
     * @param ConfigContract $config
     */
    public function __construct(
        Unsubscriber $unsubscriber,
        UnsubscriptionRepository $unsubscriptionRepository,
        ConfigContract $config
    ) {
        parent::__construct();
        $this->unsubscriber = $unsubscriber;
        $this->unsubscriptionRepository = $unsubscriptionRepository;

        $token = $config->get('services.agora.middleware.token');
        $log = new Logger('middleware');
        $log->pushHandler( new LogglyHandler('03de162f-b2bb-48ef-8d23-3da987b1e11c/tag/gawas', Logger::INFO));

        $factory = new MiddlewareFactory($log);
        $this->middleware = $factory->create($token, MiddlewareFactory::MIDDLEWARE_PRODUCTION);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting Worker Process');
        $this->unsubscriber->run($this->middleware, $this->unsubscriptionRepository);
        $this->info('Worker Process Completed');
    }
}
