<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UnsubWorkerProcess::class,
        Commands\WeeklyReport::class,
        Commands\CustomerProfileWorkerProcess::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // run command every minute
        $schedule->command('tfs:queueWorker')
            ->everyMinute();

        $schedule->command('tfs:opiumWorker')
            ->everyMinute();

        // run command every week
        $schedule->command('tfs:weeklyReport')
            ->weekly();



    }
}
