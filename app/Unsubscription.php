<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Unsubscription
 *
 * @package App
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class Unsubscription extends Model
{
    /**
     * @var string
     */
    protected $table = 'Unsubscriptions';

    /**
     * Status
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne('App\UnsubscriptionStatus');
    }

    /**
     * scopeMinutesAgo
     *
     * @param $query
     * @param $minutes
     * @return mixed
     */
    public function scopeMinutesAgo($query, $minutes)
    {
        return $query->where('created_at', '<=', Carbon::now()->subMinutes($minutes)->toDateTimeString() );
    }

    /**
     * scopeMinutesAgoAndStatus
     *
     * @param $query
     * @param $minutes
     * @param $status
     * @return mixed
     */
    public function scopeMinutesAgoAndStatus($query, $minutes, $status)
    {
        return $query->where('created_at', '<=', Carbon::now()->subMinutes($minutes)->toDateTimeString() )
                        ->where('status_id', '=', $status);
    }

    /**
     * scopeThisWeek
     *
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeThisWeek($query, $status)
    {
        return $query->where('created_at', '>=', Carbon::now()->subWeek(1)->toDateTimeString() )
                    ->where('status_id', '=', $status);
    }

    /**
     * scopeByDomainName
     *
     * @param $query
     * @param $domain
     * @return mixed
     */
    public function scopeByDomainName($query, $domain)
    {
        return $query->where('domainName', '=', $domain);
    }

    /**
     * scopeByStatus
     *
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeByStatus($query,$status)
    {
        return $query->where('status_id', '=', $status);
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * setEmailAddress
     *
     * @param $emailAddress
     * @return $this
     * @throws \Exception
     */
    public function setEmailAddress($emailAddress)
    {
        if(empty($emailAddress)){
            throw new \Exception('Invaild Email Address');
        }
        $this->emailAddress = $emailAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getListCode()
    {
        return $this->listCode;
    }

    /**
     * setListCode
     *
     * @param $listCode
     * @return $this
     */
    public function setListCode($listCode)
    {
        $this->listCode = $listCode;
        return $this;
    }


    /**
     * setDomainName
     *
     * @param $domain
     */
    public function setDomainName($domain)
    {
        $this->domainName = $domain;
    }
}