<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UnsubscriptionStatus
 *
 * @package App
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class UnsubscriptionStatus extends Model
{
    /**
     * @var string
     */
    protected $table = 'UnsubscriptionStatus';

    /**
     * Unsubscription
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function unsubscription()
    {
        return $this->hasMany('App\Unsubscription');
    }
}
