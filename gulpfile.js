var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // Compiles scss
    mix.sass('app.scss');

    // Concatinates app.css and vendor.css into 'app.css'
    mix.styles(['app.css'], 'public/css/app.css');

    // Versions footer.js, header.js, app.css
    mix.version(["public/css/app.css"]);
});
