<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace Threefold\MessageCentral\Exception;

/**
 * Message Central exception interface
 *
 * @author John Dunne <jdunne@threefoldsystems.com>
 * @package Threefold\Middleware
 */
interface MessageCentralExceptionInterface
{
}
