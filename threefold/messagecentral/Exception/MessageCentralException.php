<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace Threefold\MessageCentral\Exception;

/**
 * General MC Exception class.
 *
 * @author John Dunne <jdunne@threefoldsystems.com>
 * @package Threefold\Middleware
 */
class MessageCentralException extends \RuntimeException implements MessageCentralExceptionInterface
{
}
