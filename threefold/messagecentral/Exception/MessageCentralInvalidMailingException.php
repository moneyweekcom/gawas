<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace Threefold\MessageCentral\Exception;

/**
 * Invalid Mailing Exception class.
 *
 * @author John Dunne <jdunne@threefoldsystems.com>
 * @package Threefold\Middleware
 */
class MessageCentralInvalidMailingException extends \RuntimeException implements MessageCentralExceptionInterface
{
}
