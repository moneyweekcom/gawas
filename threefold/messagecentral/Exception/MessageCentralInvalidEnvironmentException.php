<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace Threefold\MessageCentral\Exception;

/**
 * Invalid Token Exception class.
 *
 * @author John Dunne <jdunne@threefoldsystems.com>
 * @package Threefold\Middleware
 */
class MessageCentralInvalidEnvironmentException extends \RuntimeException implements MessageCentralExceptionInterface
{
}
