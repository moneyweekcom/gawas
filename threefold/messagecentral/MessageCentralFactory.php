<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace Threefold\MessageCentral;

use Psr\Log\LoggerInterface as Logger;
use Threefold\MessageCentral\Exception\MessageCentralInvalidEnvironmentException;
use GuzzleHttp\Client;

/**
 * Middleware Factory
 *
 * @author John Dunne <jdunne@threefoldsystems.com>
 */
class MessageCentralFactory
{
    const MESSAGECENTRAL_PRODUCTION = 'production';
    const MESSAGECENTRAL_UAT = 'uat';
    const MESSAGECENTRAL_FAKE = 'fake';

    /**
     * Message Central production URL
     */
    const PRODUCTION_URL = 'https://servicegateway.agora-inc.com/';
    /**
     * Message Central UAT URL
     */
    const UAT_URL = 'https://servicegateway.agora-inc.com/';

    /**
     * Message Central Paid Stack
     */
    const PAID_STACK = 'mcs-paid/';
    /**
     * Message Central Free Stack
     */
    const FREE_STACK = 'mcs/';

    /**
     * @var Logger
     */
    protected $log;

    /**
     * Constuctor
     *
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Create Agora message central wrapper
     *
     * @param string $token Token
     * @param string $orgId Org ID
     * @param string $stack options are free or paid
     * @param string $environment (optional, default: uat) Environment (options: production|uat|fake)
     * @return MessageCentralInterface
     * @throws MessageCentralInvalidEnvironmentException If invalid environment is given
     */
    public function create($token, $orgId, $stack ='free', $environment = 'uat')
    {
        $this->log->debug('Creating new middleware', ['environment' => $environment]);

        switch ($environment) {
            case self::MESSAGECENTRAL_PRODUCTION:
                if($stack == 'paid'){
                    $guzzleClient = new Client(['base_uri' => self::PRODUCTION_URL . self::PAID_STACK]);
                }else{
                    $guzzleClient = new Client(['base_uri' => self::PRODUCTION_URL . self::FREE_STACK]);
                }

                return new MessageCentral($this->log, $guzzleClient, $token, $orgId);

            case self::MESSAGECENTRAL_UAT:
                if($stack == 'paid'){
                    $guzzleClient = new Client(['base_uri' => self::UAT_URL . self::PAID_STACK]);
                }else{
                    $guzzleClient = new Client(['base_uri' => self::UAT_URL . self::FREE_STACK]);
                }

                return new MessageCentral($this->log, $guzzleClient, $token, $orgId);

            case self::MESSAGECENTRAL_FAKE:
                $guzzleClient = new Client(['base_uri' => 'http://example.com']);
                return new MessageCentralFaker($this->log, $guzzleClient, $token, $orgId);

            default:
                $this->log->error('Invalid environment', ['environment' => $environment]);
                throw new MessageCentralInvalidEnvironmentException('Invalid environment: ' . $environment);
        }
    }
}
