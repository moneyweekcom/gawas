<?php

namespace Threefold\MessageCentral;

use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface as Logger;
use Threefold\MessageCentral\Exception\MessageCentralConnectionException;
use Threefold\MessageCentral\Exception\MessageCentralException;
use Threefold\MessageCentral\Exception\MessageCentralExceptionInterface;
use Threefold\MessageCentral\Exception\MessageCentralInvalidMailingException;
use Threefold\MessageCentral\Exception\MessageCentralInvalidOrgIdException;
use Threefold\MessageCentral\Exception\MessageCentralInvalidTokenException;

class MessageCentral implements MessageCentralExceptionInterface
{
    /**
     * Object container for the logging system.
     *
     * @var Logger
     */
    protected $log;
    /**
     * @var string The token used to authenticate with the rest service. Inserted into the header as a value for 'token:'
     */
    protected $token;
    /**
    * @var string The url used for Message Central Calls
    */
    protected $url;
    /**
     * @var string The orgId
     */
    protected $orgId;
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * Constructor
     *
     * @param Logger $log Logger
     * @param ClientInterface $httpClient
     * @param string $token
     * @param string $orgId
     */
    public function __construct(Logger $log, ClientInterface $httpClient, $token, $orgId)
    {
        $this->log = $log;
        $this->token = $token;
        $this->httpClient = $httpClient;
        $this->orgId = $orgId;

    }

    /**
     * Trigger a Message Central Mailing
     *
     * @param string $email - email to send to
     * @param string $content - email content
     * @param string $mailID - Id of mailing to send
     * @return array response from message central request
     */

    public function putTriggerMailing($email, $content,$mailID)
    {
        $url = 'mailing/trigger/orgid/' . $this->orgId . '/mid/'. $mailID;
        $payload = array('email' => $email, 'context' => array ('email_body' => $content));
        return $this->post($url, $payload);
    }


    /**
     * Make GET request
     *
     * THIS IS NOT TESTED....
     *
     * A helper method to reduce repetition.
     *
     * @param string $url
     * @return array Associative array of returned data.
     * @throws MessageCentralException If invalid response status code received
     * @throws MessageCentralInvalidTokenException If invalid token is used to make call
     */
    protected function UNTESTEDget($url)
    {
        $this->log->info('Message Central GET Request to: ' . $url . ' (token: ' . $this->token . ')');
        try {
            // Make request
            $headers = ['token' => $this->token];
            $response = $this->httpClient->request(
                'GET',
                $url,
                ['headers' => $headers]
            );
            $statusCode = $response->getStatusCode();
            $contents = $response->getBody()->getContents();
            // Check result
            switch ($statusCode) {
                case 200:
                    // This is a successful call and will return a php object
                    $this->log->info('Response contents', (array)$contents);
                    return $contents;
                case 204:
                    // No content - no results found
                    return false;
                default:
                    $this->log->error('Response contents', (array)$contents);
                    throw new MessageCentralException('Bad result. Status Code: ' . $statusCode);
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            switch ($e->getResponse()->getStatusCode()) {
                // Invalid token exception
                case 403:
                    throw new MessageCentralInvalidTokenException('Invalid token');
                default:
                    // Rethrow everything else
                    throw $e;
            }
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            // Message Central error
            if ($e->getResponse()->getStatusCode() == 500
                && strpos($e->getResponse()->getBody(), 'Failed Message Central connection') !== false
            ) {
                throw new MessageCentralConnectionException('Unable to connect to  Message Central', null, $e);
            }
        }
    }

    /**
     * Helper Method for POST requests
     *
     * A helper method to reduce repetition.
     *
     * @param string $url
     * @return array Associative array of returned data.
     * @throws MessageCentralException If invalid response status code received
     * @throws MessageCentralInvalidTokenException If invalid token is used to make call
     */
    private function post($url, $payload, $headers = array())
    {
        $this->log->info('Message Central POST Request to: ' . $url);
        try {

            $headers['token'] = $this->token;
            $headers['Content-Type'] = 'application/json';

            $response = $this->httpClient->request(
                'POST',
                $url,
                ['body' => json_encode($payload),'headers' => $headers]
            );
            $statusCode = $response->getStatusCode();
            $contents = $response->getBody()->getContents();
            $this->log->info('Response from call',['response' => $response, 'url' => $url, 'payload'=> $payload]);
            // Check result
            switch ($statusCode) {
                case 200:
                    // This is a successful call and will return a php object
                    $this->log->info('Response contents', (array)$contents);
                    return $contents;
                case 204:
                    // No content - no results found
                    return false;
                default:
                    $this->log->error('Response contents', (array)$contents);
                    throw new MessageCentralException('Bad result. Status Code: ' . $statusCode);
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            switch ($e->getResponse()->getStatusCode()) {
                case 403:
                    if (strpos($e->getResponse()->getBody(), 'owning org') !== false) {
                        throw new MessageCentralInvalidOrgIdException('Invalid Org Id');
                    }else{
                        throw new MessageCentralInvalidTokenException('Invalid token');
                    }
                case 400:
                    if (strpos($e->getResponse()->getBody(), 'no such mailing') !== false) {
                        throw new MessageCentralInvalidMailingException('No Such Mailing');
                    }else{
                        throw new MessageCentralConnectionException('Unable to connect to  Message Central', null, $e);
                    }
                default:
                    // Rethrow everything else
                    throw $e;
            }
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            // Message Central error
            if ($e->getResponse()->getStatusCode() == 400) {
                if (strpos($e->getResponse()->getBody(), 'no such mailing') !== false) {
                    throw new MessageCentralInvalidMailingException('No Such Mailing');
                }else{
                    throw new MessageCentralConnectionException('Unable to connect to  Message Central', null, $e);
                }
            }
            throw $e;
        }
    }
}