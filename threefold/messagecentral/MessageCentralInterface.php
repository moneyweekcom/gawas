<?php
/**
 *  Copyright (C) Threefold systems - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 */

namespace Threefold\MessageCentral;

use GuzzleHttp\ClientInterface;
use \Psr\Log\LoggerInterface as Logger;
use Threefold\MessageCentral\Exception\MessageCentralInvalidTokenException;
/**
 * Interface MessageCentralInterface
 * @package Threefold\Middleware
 */
interface MessageCentralInterface
{
    /**
     * Constructor
     *
     * @param Logger $log Logger
     * @param ClientInterface $httpClient
     * @param string $token
     * @param string $orgId
     */
    public function __construct(Logger $log, ClientInterface $httpClient, $token, $orgId);

    /**
     * Trigger a Message Central Mailing
     *
     * @param string $email - email to send to
     * @param string $content - email content
     * @param string $mailID - Id of mailing to send
     * @param array $headers - headers to send
     */

    public function putTriggerMailing($email, $content,$mailID);

    /**
     * Make GET request
     *
     * A helper method to reduce repetition.
     *
     * @param string $url
     * @return array Associative array of returned data.
     * @throws MessageCentralException If invalid response status code received
     * @throws MessageCentralInvalidTokenException If invalid token is used to make call
     */
    public function get($url);

    /**
     * Helper Method for POST requests
     *
     * A helper method to reduce repetition.
     *
     * @param string $url
     * @return array Associative array of returned data.
     * @throws MessageCentralException If invalid response status code received
     * @throws MessageCentralInvalidTokenException If invalid token is used to make call
     */
    public function put($url);

}
