<!DOCTYPE html>
<html lang="en">
<head>
    @include($template.'.includes.head')
</head>
<body id="{{$template}}">
    @if(View::exists($template.'.includes.'.$listCode))
        @include($template.'.includes.'.$listCode)
    @else
        @include($template.'.includes.header')
    @endif
<div class = "container" id="ajax-target">
    @yield('content')
</div>
@include($template.'.includes.footer')
</body>
</html>