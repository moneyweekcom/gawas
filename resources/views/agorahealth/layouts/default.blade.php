<!DOCTYPE html>
<html lang="en">
<head>
    @include($template.'.includes.head')
</head>
<body id="{{$template}}">
    @include($template.'.includes.header')
<div class = "container" id="ajax-target">
    @yield('content')
</div>
@include($template.'.includes.footer')
</body>
</html>