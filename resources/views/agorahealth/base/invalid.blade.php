@extends($template.'.layouts.default')
@section('content')

    <div id="success" class="row">
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-danger">
                <p>We're sorry, it looks like there's a problem with the link you used.</p>
            </div>
        </div>

    </div>

@stop
