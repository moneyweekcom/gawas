@extends($template.'.layouts.default')
@section('content')

<div class="row">

    @if(View::exists($template.'.templates.'.$listCode))
        @include($template.'.templates.'.$listCode)
    @else
        @include($template.'.templates.list')
    @endif

    <div class="col-lg-12 col-md-12">
            <div id ="unsubscribe">
                    <form class="form-signin" method="POST" action="/">

                        {!! csrf_field() !!}
                        <input type="hidden" name="listCode" value="{{$listCode}}">
                        <input type="hidden" name="listName" value="{{$listName}}">
                        <div>
                            <p>
                                We're sorry to see you go!
                            </p>

                            @if (empty($emailAddress) || (!empty($emailAddress) && !empty($response)))
                                <p>
                                    To unsubscribe from {{$listName}} simply enter your email address in the space below
                                    and click the "Unsubscribe" button to be removed from our mailing list.
                                </p>
                            @else
                                <p>
                                    To unsubscribe from {{$listName}} simply click the "Unsubscribe" button below to be
                                    removed from our mailing list.
                                </p>
                            @endif

                        </div>

                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if(Session::has('listCode'))
                            <div class="alert alert-danger">
                                {{ Session::get('listCode') }}
                            </div>
                        @endif

                        @if (empty($emailAddress))
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="email" id="email" class="form-control  {{($response ? 'invalid_email':'')}}"
                                       name="emailAddress" value="" placeholder="Email address"  autofocus> <br />
                            </div>
                        @elseif (!empty($emailAddress) && !empty($response))
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="email" id="email" class="form-control  {{($response ? 'invalid_email':'')}}"
                                       name="emailAddress" value="{{$emailAddress}}" placeholder="Email address"  autofocus> <br />
                            </div>
                        @else
                            <input type="hidden" id="email" name="emailAddress" value="{{$emailAddress}}">
                        @endif

                        <div class="controls">
                            <button class="btn btn-lg btn-success btn-block" formnovalidate="formnovalidate" type="submit">Unsubscribe</button>
                        </div>
                    </form>
                </div>
    </div>

</div>

@stop
