@extends($template.'.layouts.default')
@section('content')

    <div id="success" class="row">
        <div class="col-lg-12 col-md-12">
            <div>
                @if(Session::has('success'))
                    <span class="success">
                         <i class="glyphicon glyphicon-ok"></i>
                            {{ Session::get('success') }}
                    </span>
                @endif
            </div>
        </div>

    </div>

@stop