<head>
    @include($template.'.emails.emailHead')
</head>
<body style="padding: 0; margin: 0 auto; min-width: 100%;">
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; padding:0px; margin:0 auto;" width="100%" bgcolor="#343a40">

    <!--15px Spacer-->
    <tr>
        <td align="center" style="font-size: 10px; line-height: 10px; margin:0 auto; color:#343a40;" height="15">.</td>
    </tr>

    <!--Main Post Body-->
    <tr>
        <td align="center" style="font-size: 16px; line-height: 20px; margin:0 auto;">
            <table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; max-width:700px; word-break: break-word; margin:0 auto;" width="700">

                <tr>

                    <td style="border-collapse: collapse; max-width:700px; word-break: break-word; margin:0 auto; border-radius:4px;" bgcolor="#ffffff" width="700">

                        <table align="left" cellpadding="0" cellspacing="0" style="border-collapse: collapse; word-break: break-word; margin:0 auto; width:100%;">


                        @include($template.'.emails.emailHeader')

                            @yield('content')

                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--15px Spacer-->
    <tr>
        <td align="center" style="font-size: 10px; line-height: 10px; margin:0 auto; color:#343a40;" height="15">.</td>
    </tr>

    @include($template.'.emails.emailFooter')

    </table>
</body>