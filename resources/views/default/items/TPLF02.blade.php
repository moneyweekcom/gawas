<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#999999">
    <tr>
        <td style="border-collapse: collapse; text-align: left; padding-top:2px;">

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#dddddd">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;">

                        <!--Product Title-->
                        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" align="left">
                            <tr>
                                <td style="border-collapse: collapse;">
                                    <strong>Tim Price's 2017 Financial Game-Plan</strong>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#eeeeee">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        You can access your report by clicking the link below and entering the password provided.<br><br>

                        <strong><a href="https://www.southbankresearch.com/wp-content/uploads/sites/9/2016/12/Your-2017-Financial-Game-Plan.pdf">
                                Tim Price's 2017 Financial Game-Plan
                            </a></strong><br><br>

                        #Password: gameplan
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

