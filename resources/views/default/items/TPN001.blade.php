<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#999999">
    <tr>
        <td style="border-collapse: collapse; text-align: left; padding-top:2px;">

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#dddddd">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;">

                        <!--Product Title-->
                        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" align="left">
                            <tr>
                                <td style="border-collapse: collapse;">
                                    <strong>Trigger Point Trader</strong><br>
                                    &pound;595 for a one-year subscription, renewing at &pound;1,200 after your second year (by Direct Debit)
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#eeeeee">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        You can access all your research reports from the <em>Trigger Point Trader</em> subscriber area - <a href="https://www.southbankresearch.com/trigger-point-trader-publication/reports/">which you can find here</a>.<br><br>

                        I'll post your copy of <em>Crowd Money</em> straight away. It should arrive within 10 working days.<br><br>

                        You can also view an exclusive video introducing you to Eoin’s strategy here:<br><br>

                        <strong><a href="https://www.southbankresearch.com/trigger-point-trader-publication/">The Secrets of Trigger Point Trader</a></strong>

                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

