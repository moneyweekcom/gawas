<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#999999">
    <tr>
        <td style="border-collapse: collapse; text-align: left; padding-top:2px;">

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#dddddd">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;">

                        <!--Product Title-->
                        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" align="left">
                            <tr>
                                <td style="border-collapse: collapse;">
                                    <strong>London Investment Alert</strong><br>
                                    &pound;45 for a one-year subscription, renewing at &pound;90 annually after your first year (by Direct Debit)
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#eeeeee">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        You can access your report by clicking the link below and using your log in details from the bottom of this email.<br><br>

                        <strong><a href="https://www.southbankresearch.com/wp-content/uploads/sites/9/2016/12/LIA_The_FailSafe_Plan_2016.pdf">
                                The Failsafe Plan: Your Total Wealth Building Package
                            </a></strong>

                    </td>
                </tr>

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        <strong><a href="https://www.southbankresearch.com/wp-content/uploads/sites/9/2016/11/LIA_LTW_Portfolio_Premia_2016.pdf">
                                The Lifetime Wealth Portfolio
                            </a></strong>

                    </td>
                </tr>

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        I'll post your copy of <em>Investing Through the Looking Glass</em> straight away. It should arrive within 10 working days

                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

