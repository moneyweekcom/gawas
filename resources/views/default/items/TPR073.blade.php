<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#999999">
    <tr>
        <td style="border-collapse: collapse; text-align: left; padding-top:2px;">

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#dddddd">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;">

                        <!--Product Title-->
                        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" align="left">
                            <tr>
                                <td style="border-collapse: collapse;">
                                    <strong>The Price Report</strong><br>
                                    &pound;594 for a one-year subscription, renewing at &pound;994 annually after your first year (by Credit Card)
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>

            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#eeeeee">

                <tr>
                    <td style="border-collapse: collapse; padding:10px;" class="reportLinks">

                        You can access your report by clicking the link below and using your log in details from the bottom of this email.<br><br>

                        <strong><a href="https://www.southbankresearch.com/wp-content/uploads/sites/9/2016/08/How-to-be-an-Intelligent-Investor_19-08-2016.pdf">
                                The Rite of Wealth: Three Investments to start your Portfolio
                            </a></strong><br><br>

                        I'll post your copy of <em>The Intelligent Investor</em> with my annotations out straight away. It should arrive within 10 working days.

                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

