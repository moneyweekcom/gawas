<td valign="top" colspan="2" class="bodyContent itemContent" style="padding-right:0;">
    <table width="100%" class="item-head" style="background-color: #e3e3e3;border-top: 3px solid #cccccc;">
        <tr>
            <td valign="top" >
                <p><strong>{{$order['itemName']}}</strong></p>
                <p>2 Year Subscription</p>
            </td>
            <td align="right">
                <p><strong>{{$order['itemPrice']}}</strong></p>
            </td>
        </tr>
    </table>
    <table width="100%" class="item-body"  style="background-color: #cccccc;">
        <tr>
            <td valign="top" >
                <p><strong>View your reports by clicking the links below:</strong></p>

                <p><a href="http://www.google.com" class="item-link">MY #1 Advantage</a></p>
                <p>How the momentum effect could boost your returns for the next decade...</p>

                <p><a href="http://www.google.com" class="item-link">Fleet Street Letter Legacy Edition</a></p>
                <p>A summary of The Fleet Street Letter's past, featuring cover stories from history's most eventful
                    moments, with the odd particularly intriguing article...</p>
            </td>
        </tr>
    </table>
</td>

