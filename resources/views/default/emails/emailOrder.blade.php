@extends($template.'.layouts.emailTemplate')
@section('content')

<tr>
    <td style="border-collapse: collapse; margin:0 auto; padding:20px; text-align: left;" class="blueLinks">

        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;">
            <tr>
                <td style="border-collapse: collapse; text-align: left;">
                    <p class="dear_line">Dear {{$customerName}},</p>

                    <p>
                        Hello, and welcome to <em>Southbank Investment Research</em>, I'm Dan Denning - the Publisher.
                    </p>

                    <p>
                        This is just a short note to let <strong>you know your order has been confirmed</strong> and to show you how to collect your research.
                    </p>

                    <p>
                        You have taken an important step toward becoming a more intelligent and informed investor - and, hopefully, a more profitable one too.
                    </p>

                    <p>
                        Our goal here is simple: <span class="underline">to publish investment ideas that can change your life for the better</span>.
                    </p>

                    <p>
                        I believe that better research leads to better investing. Better investing leads to financial independence.
                        And financial independence allows you to live a better, richer, and freer life.
                    </p>

                    <p>
                        That's what you can expect from us at <em>Southbank Investment Research</em>.
                    </p>

                    <p>
                        Please find your order details below and everything you need to get started.
                    </p>

                    <p>
                        <img src="http://images.moneyweek.com/sigs/sig_dan-denning.gif">
                    </p>

                    <p>Dan Denning</p>
                    <p>Publisher, Southbank Investment Research</p>
                </td>
            </tr>
        </table>

        @foreach ($orders as $order)
             <!--Start Product Layout-->
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" width="100%" bgcolor="#999999">
                    <tr>
                        <td style="border-collapse: collapse; text-align: left; padding-top:2px;">
                            @include($template.'.items.'.$order['template'])
                        </td>
                    </tr>
                </table>

        <!--15px spacer-->
        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" width="100%">
            <tr><td height="15"></td></tr>
        </table>
        @endforeach

                <!--Product Total-->
        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="right">
            <tr>
                <td style="border-collapse: collapse; padding:10px; text-align: right;">Delivery:</td>
                <td style="border-collapse: collapse; padding:10px; text-align: right; font-weight:600;">{{$delivery}}</td>
            </tr>
            <tr>
                <td style="border-collapse: collapse; padding:10px; text-align: right;">Subtotal:</td>
                <td style="border-collapse: collapse; padding:10px; text-align: right; font-weight:600;">{{$total}}</td>
            </tr>
        </table>

        <br><br>
    </td>
</tr>
@stop

