<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Confirmation Email</title>
<style type="text/css">
    html {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
        background:#343a40;
    }

    body {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
        background-color: #eeeeee;
        font-family: Verdana, Arial, sans-serif;
        margin:0 auto;
        background:#343a40;
    }

    table {
        border-collapse: collapse;
        mso-table-lspace: 0;
        mso-table-rspace: 0;
        margin:0 auto;
        font-family: Verdana, Arial, sans-serif;
        text-align: left;
    }

    table td    {
        font-family: Verdana, Arial, sans-serif;
        font-size: 15px;
        line-height: 20px;
    }

    ul {
        margin-top: 0;
        padding-top: 15px;
    }

    em  {
        font-weight: 400;
    }

    hr  {
        color:#cccccc;
        height:1px;
        width:100%;
        background:#cccccc;
        box-shadow: none;
        border:none;
    }

    a   {
        color:#ffffff;
        text-decoration:underline;
    }

    .blueLinks a    {
        color:blue;
        text-decoration: none;
    }

    .reportLinks a  {
        color:#268dd4;
        text-decoration: none;
        font-weight:600;
    }

    .postLink   {
        font-family: Verdana, Arial, sans-serif;
        display: block;
        text-align: center;
        background: #343a40;
        font-weight: 600;
        padding: 8px 0;
        border-radius: 4px;
        font-size: 14px;
        line-height: 14px;
        width:100%;
    }
    .postLink a {
        color:#ffffff;
        text-decoration: none;
    }

    img {
        max-width:100%;
        max-width:100% !important;
        border: none;
    }

    h1, h2, h3  {
        font-family: Verdana, Arial, sans-serif;
        font-weight:700;
        color:#343a40;
        letter-spacing: -1px;
    }

    h1  {font-size:32px; line-height: 36px; margin:0 0 10px 0; }
    h2  {font-size:24px; line-height: 28px; margin:0 0 15px 0; }
    h3  {font-size:22px; line-height: 26px; margin:0 0 15px 0; }

    .responsiveImg  {
        max-width: 100%;
    }

    .author {
        color:#dd2026;
        text-transform:uppercase;
        font-weight: 600;
        font-size: 14px;
    }
    .date   {
        color:#555555;
        font-size: 14px;
        font-weight: 600;
    }


    @media all and (max-width: 720px) {

        body[yahoo] .responsive-container {
            width: 100% !important;
            max-width: 700px !important;
        }

        body[yahoo] .responsive {
            width: 100% !important;
            height: auto !important;
        }

        body[yahoo] .responsive-content h1 {
            font-size: 30px !important;
            line-height: 28px !important;
        }

        html {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0;
            mso-table-rspace: 0;
            width:100%;
        }

        .responsive-container   {
            max-width: 700px !important;
            width:100% !important;
        }

        .logo td, .alignCenterMob   {text-align: center !important;}

        .nav    {
        }


    }

    @media all and (max-width: 700px) {

        .dt_only    {
            display: none !important;
            overflow: hidden;
            font-size: 0;
            max-height: 0;
            line-height: 0;
            mso-hide: all;
        }

        .signoff_align  {
            text-align: center;
        }

        .height-fix {
            height:0 !important;
        }

        table   {
            width:100% !important;
        }

        .headDate td    {
            padding: 10px 0 !important;
        }

        .addSpacerBottom    {
            margin-bottom:20px !important;
        }

    }

    @media all and (max-width: 330px) {

        body[yahoo] .responsive-content {
            padding-left: 10px !important;
            padding-right: 10px !important;
        }

        html {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

    }

</style>
