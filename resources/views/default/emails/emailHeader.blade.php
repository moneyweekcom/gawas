<tr>

    <td style="border-collapse: collapse; margin:0 auto; padding:20px; text-align: left;" class="blueLinks">



        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: left;" align="left">
            <tr>
                <td style="border-collapse: collapse;">
                    <img src="http://southbankresearch.com/wp-content/uploads/sites/9/2016/11/SIR_email_logo.png" alt="logo" class="responsiveImg">
                </td>
            </tr>
        </table>

        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: right;" align="right" width="370">
            <tr>
                <td style="border-collapse: collapse; padding-top:10px; font-size:13px;">
                    <a href="http://www.southbankresearch.com/my-account/" style="font-weight: 600; color:#000000;">Your Account</a><br>
                    <hr><br>
                    <strong style="font-size:18px; letter-spacing:-1px;">Order Confirmation</strong>
                    <br>
                    @if (!empty($customer['orderNumber']))
                        Order Number: <span style="color:#268dd4; font-weight:600;">{{$customer['orderNumber']}}</span><br>
                    @endif
                    Promotion Code: <span style="color:#268dd4; font-weight:600;">{{$customer['promoCode']}}</span>
                </td>
            </tr>
        </table>

    </td>

</tr>