<!--Login Box-->
<tr>
    <td style="border-collpase: collapse;" align="center">

        <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; margin:0 auto; border-radius:4px;" bgcolor="#cccccc" width="700">

            <tr>

                <td style="border-collapse: collapse; padding:15px;">


                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#ffffff;" align="left">
                        <tr>
                            <td style="border-collapse: collapse; padding:4px 0 10px 0; color:#000000; font-weight:600;">
                                Website Login Details:
                            </td>
                        </tr>
                    </table>

                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#ffffff;" align="right">
                        <tr>
                            <td style="border-collapse: collapse; color:#ffffff; text-align: right;">
                                <a href="http://www.southbankresearch.com/my-account/" style="color:#000000; text-decoration: none;">
                                    <img src="http://southbankresearch.com/wp-content/uploads/sites/9/2016/11/loginBtn.png" alt="Login">
                                </a>
                            </td>
                        </tr>
                    </table>

                    <!--Login details / dark-blue box-->
                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#ffffff;" width="100%">

                        <tr>
                            <td style="border-collapse: collapse;">

                                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#cccccc;" width="100%">
                                    <tr>
                                        <td style="border-collapse: collapse; color:#cccccc; font-size:10px; line-height:10px;" height="10">.</td>
                                    </tr>
                                </table>

                                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; border-radius:4px; color:#ffffff;" width="100%" bgcolor="#555555" class="alignCenterMob">

                                    <tr>
                                        <td style="border-collapse: collapse; border-radius: 4px;">

                                            <table cellpadding="0" cellspacing="0" border="0" style="" align="left">

                                                <tr>
                                                    <td style="background: #196090; height:40px; width:40px; text-align: center; border-radius: 4px 0 0 4px;"><img src="http://southbankresearch.com/wp-content/uploads/sites/9/2016/11/user-icon.png" width="12"></td>
                                                </tr>

                                            </table>

                                            <table cellpadding="0" cellspacing="0" border="0" style="" align="left">

                                                <tr>
                                                    <td style="font-weight:600; font-size:14px; padding:10px; color:#ffffff;">Your Username is: {{$email}}</td>
                                                </tr>

                                            </table>

                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>

                    </table>


                    <!--15px Spacer-->
                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                        <tr>
                            <td align="center" style="font-size: 10px; line-height: 10px; margin:0 auto; color:#cccccc;" height="10">.</td>
                        </tr>
                    </table>


                    <!--Login details / dark-blue box-->
                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#ffffff;" width="100%">

                        <tr>
                            <td style="border-collapse: collapse;">

                                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; border-radius:4px; color:#ffffff;" width="100%" bgcolor="#555555" class="alignCenterMob">

                                    <tr>
                                        <td style="border-collapse: collapse; border-radius: 4px; font-weight:600;">

                                            <table cellpadding="0" cellspacing="0" border="0" style="" align="left">

                                                <tr>

                                                </tr><td style="background: #196090; height:40px; width:40px; text-align: center; border-radius: 4px 0 0 4px;"><img src="http://southbankresearch.com/wp-content/uploads/sites/9/2016/11/lock-icon.png" width="14"></td>

                                            </table>

                                            <table cellpadding="0" cellspacing="0" border="0" style="" align="left">

                                                <tr>
                                                    <td style="font-weight:600; font-size:14px; padding:10px; color:#ffffff;">
                                                        Forgotten your password? <a href="http://www.southbankresearch.com/help-guide/" style="color:#ffffff; font-size:14px;">Click here to reset.</a>
                                                    </td>
                                                </tr>

                                            </table>



                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td style="border-collapse: collapse; padding:5px 15px 15px 15px;">

                    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color:#ffffff;" width="100%">

                        <tr>
                            <td style="border-collapse: collapse; font-size:13px; line-height:18px; text-align: left; color:#000000;">
                                If you have any problems on the website, our Customer Services team will be happy to help you. Just call on 0207 633 3784 (Monday - Friday, 9.00am - 5.30pm)
                            </td>
                        </tr>

                    </table>




                </td>

            </tr>

        </table>

    </td>
</tr>

<!--Disclaimer-->
<tr>
    <td align="center" style="font-size: 16px; line-height: 20px; margin:0 auto;">
        <table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; max-width:700px; word-break: break-word; margin:0 auto;" width="700">

            <tr>

                <td style="border-collapse: collapse; max-width:700px; word-break: break-word; margin:0 auto; border-radius:4px;" width="660">

                    <table align="left" cellpadding="0" cellspacing="0" style="border-collapse: collapse; word-break: break-word; margin:0 auto; width:100%;">

                        <tr>

                            <td style="border-collapse: collapse; margin:0 auto; padding:20px; color:#ffffff; font-size:14px; line-height:18px; text-align: left;">

                                Your capital is at risk when you invest in shares - you can lose some or all of your money, so never risk more than you can afford to lose. Past performance and forecasts are not a reliable indicator of future results. Bid offer spreads, commissions, fees and other charges can reduce returns from investments. Some recommendations may be denominated in a currency other than sterling. The return from these may increase or decrease as a result of currency fluctuations. Any dividends will be taxed at source in the country of issue.<br><br>

                                Profits from share dealing are a form of capital gain and subject to taxation. Tax treatment depends on individual circumstances and may be subject to change in the future.<br><br>

                                Investment Director: Eoin Treacy. Editors or contributors may have an interest in shares recommended. The information and opinions expressed do not necessarily reflect the views of other editors/contributors of Southbank Investment Research Limited. Full details of our complaints procedure and terms &amp; conditions can be found on our website <a style="color:white" href="http://www.southbankresearch.com">southbankresearch.com</a>.<br><br>

                                Frontier Tech Investor and Trigger Point Trader are issued by Southbank Investment Research Ltd. Registered in England and Wales No 9539630. VAT No GB629 7287 94. Registered Office: 8th Floor Friars Bridge Court, 41-45 Blackfriars Road, London SE1 8NZ.<br><br>

                                Southbank Investment Research Limited is authorised and regulated by the Financial Conduct Authority. FCA No 706697. <a style="color:white" href="https://register.fca.org.uk/">https://register.fca.org.uk/</a>.<br><br>

                                &copy; 2016 Southbank Investment Research Ltd.<br><br>

                                <strong>Contact Us</strong><br><br>

                                To contact customer services, please click here. Alternatively, telephone us on 020 7633 3622, Monday to Friday, 9.00am - 5.30pm.<br><br>

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

        </table>
    </td>
</tr>