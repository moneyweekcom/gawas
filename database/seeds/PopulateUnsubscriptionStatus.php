<?php

use Illuminate\Database\Seeder;

class PopulateUnsubscriptionStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('UnsubscriptionStatus')->insert(array(
            array('status'=>'Queue 1'),
            array('status'=>'Queue 2'),
            array('status'=>'Queue 3'),
            array('status'=>'Queue 4'),
            array('status'=>'Complete'),
            array('status'=>'Error Advantage'),
            array('status'=>'Out of Attempts')
        ));
    }
}
