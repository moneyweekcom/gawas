<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier', 20);
            $table->text('profile');
            $table->integer('processed')->default(0);
            $table->text('messageCentralResponse');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id','identifier', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_profiles');
    }
}
