<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnsubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Unsubscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emailAddress');
            $table->integer('status_id')->unsigned()->default(1);
            $table->string('listCode', 255);
            $table->text('adv_response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('Unsubscriptions');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
