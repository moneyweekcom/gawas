#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install build-essential
wget http://download.redis.io/releases/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
cd redis-stable
make
make test
make install
cd utils
sudo ./install_server.sh

echo "Start the Redis server: sudo service redis_6379 start

6379 is the port and will depend on your port option during install.
Stop the Redis server: sudo service redis_6379 stop

6379 is the port and will depend on your port option during install.
Access the Redis CLI: redis-cli"