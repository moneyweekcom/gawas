<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'agora' => [
        'middleware' => [
            'token' => env('MIDDLEWARE_TOKEN'),
            'validListCodes' => explode(',', env('VALID_LIST_CODES')),
            'validListNames' => explode(',', env('VALID_LIST_NAMES')),
        ],
        'messagecentral' => [
            'token' => env('MESSAGECENTRAL_TOKEN'),
            'env' => env('MESSAGECENTRAL_ENVIRONMENT'),
            'orgID' => env('MESSAGECENTRAL_ORGID'),
            'stack' => env('MESSAGECENTRAL_STACK'),
            'mailID' => env('MESSAGECENTRAL_MAILID'),
        ],
        'toemails' => [
            'agorafinancial' => env('AGORA_FINANCIAL_EMAILS'),
            'agorahealth' => env('AGORA_HEALTH_EMAILS'),
<<<<<<< HEAD
            'moneyweekresearch' => env('MONEYWEEK_RESEARCH_EMAILS'),
            'southbankresearch' => env('SOUTH_BANK_RESEARCH_EMAILS'),
=======
            'southbankresearch' => env('SOUTHBANK_RESEARCH_EMAILS'),
>>>>>>> 8e100927282a6da641401263918b6c02fdaef62d
            'default' => env('DEFAULT_EMAILS'),
        ],
        'ccmails' => env('CC_ON_ALL_MAILS'),
        'orderEmailSender' => env('ORDER_EMAIL_SENDER')

    ]

];
